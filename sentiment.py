#!/usr/local/bin/python3.4
# coding: utf-8
import string
import re
#import nltk
#from nltk import word_tokenize

# Removes URLs.
def remove_urls(tweet):
	t = ' '.join(re.sub("(\w+:\/\/\S+)"," ",tweet).split())
	return t

# Removes username tokens.
def remove_usernames(tweet):
	t = ' '.join(re.sub("(@[A-Za-z0-9]+)"," ",tweet).split())
	return t

# Removes hashtag symbols.
def remove_hashtags(tweet):
	for token in tweet.split(): 
		if token.startswith("#"):
			token.replace('#','')
	return tweet

# Removes punctuation, but keeps emoticons.
def remove_punctuation(tweet):
	# Reading positive emoticons and adding them to the pos list.
	with open('pos_emoticons.txt', 'rt', encoding='utf-8') as pos_emo:
		positive_emoticons = []
		for emoticon in pos_emo:
			positive_emoticons.append(emoticon)
	pos_emo.close()

	# Reading negative emoticons and adding them to the neg list.
	with open('neg_emoticons.txt', 'rt', encoding='utf-8') as neg_emo:
		negative_emoticons = []
		for emoticon in neg_emo:
			negative_emoticons.append(emoticon)
	neg_emo.close()
	
	# Converting emoticons to keyword.
	for token in tweet.split():
		if token in positive_emoticons:
			tweet.replace(token, 'POSEMO')
		elif token in negative_emoticons:
			tweet.replace(token, 'NEGEMO')
	
	punctuation_marks = set(string.punctuation)	
	t = ''.join(ch for ch in tweet if ch not in punctuation_marks)
	
	return t

# Removes stop words.
def remove_stopwords(tokens):
	for token in tokens:
		if token in stopwords:
			tokens.remove(token)
	return tokens

# Reading positive words and adding them to a list.
with open('pos.txt', 'rt', encoding='utf-8') as pos:
	positive_words = []
	for word in pos:
		positive_words.append(word.rstrip())
	pos.close()

# Reading negative words and adding them to a list.
with open('neg.txt', 'rt', encoding='utf-8') as neg:
	negative_words = []
	for word in neg:
		negative_words.append(word.rstrip())
	neg.close()

# Reading stop words and adding them to a list.
with open('sw.txt', 'rt', encoding='utf-8') as sw:
	stopwords = []
	for word in sw:
		stopwords.append(word)
	sw.close()

# Reading file with tweets.
with open('tweets.txt', 'rt', encoding='utf-8') as tweets:

	for tweet in tweets:
		# removing URLs
		tweet = remove_urls(tweet)
		# removing usernames
		tweet = remove_usernames(tweet)
		# removing hashtags
		tweet = remove_hashtags(tweet)
		# removing punctuation marks
		tweet = remove_punctuation(tweet)
		
		# transforming the tweet to lower case
		tweet = tweet.lower()
		# tokenizing the tweet
		raw_tokens = tweet.split(' ')
		# removing stop words
		tokens = remove_stopwords(raw_tokens)
		# alternative NLTK function. watch out on how it tokenizes punctuation symbols.
		#raw_tokens = word_tokenize(tweet)
		
		pos_count = 0
		neg_count = 0
		
		for token in tokens:
			# counting positive words
			if token in positive_words:
				pos_count += 1
			# counting negative words
			elif token in negative_words:
				neg_count += 1
		
		# classifying the tweet
		if pos_count > neg_count:
			#print(tweet, ", \'positive\'")
			result = ''.join((tweet,", \'positive\'"))
		elif pos_count < neg_count:
			#print(tweet, ", \'negative\'")
			result = ''.join((tweet,", \'negative\'"))
		else:
			#print(tweet, ", \'neutral\'")
			result = ''.join((tweet,", \'neutral\'"))
		
		with open("results.txt", "a+") as r:
			print("%s" % result, file=r)
		
print("good bye!")